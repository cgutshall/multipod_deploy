name := "base-service"
organization := "com.revealmobile"
version := "0.0.12-SNAPSHOT"
scalaVersion := "2.12.6"
parallelExecution in ThisBuild := false

updateConfiguration in updateSbtClassifiers := (updateConfiguration in updateSbtClassifiers).value.withMissingOk(true)

lazy val versions = new {
  val finatra = "19.5.1"
  val aws = "1.11.416"
  val elastic = "7.1.0"
  val model = "0.3.13-SNAPSHOT"
  val slick = "3.3.0"
  val specs = "4.5.1"
}
libraryDependencies ++= Seq(
  // configuration
  "com.typesafe" % "config" % "1.3.3",

  // model
  "com.revealmobile" %% "model" % versions.model,

  // rest
  "com.twitter" %% "finatra-http" % versions.finatra,
  "com.twitter" %% "inject-request-scope" % versions.finatra,

  // pipeline
  "org.apache.kafka" % "kafka-clients" % "2.2.0",
  "com.twitter" %% "finatra-kafka-streams" % versions.finatra,

  // logging
  "ch.qos.logback" % "logback-classic" % "1.2.3",

  // cloud
  "com.amazonaws" % "aws-java-sdk-ec2" % versions.aws,
  "com.amazonaws" % "aws-java-sdk-s3" % versions.aws,

  // mail
  "com.amazonaws" % "aws-java-sdk-ses" % versions.aws,
  "com.sun.mail" % "javax.mail" % "1.6.2",

  // security
  "com.pauldijou" %% "jwt-core" % "1.0.0",
  "com.google.crypto.tink" % "tink" % "1.3.0-rc1",
  "com.amazonaws" % "aws-java-sdk-secretsmanager" % versions.aws,

  // sftp
  "com.jcraft" % "jsch" % "0.1.55",

  // persistence
  "com.typesafe.slick" %% "slick" % versions.slick,
  "com.typesafe.slick" %% "slick-hikaricp" % versions.slick,
  "com.github.tminglei" %% "slick-pg" % "0.17.2",
  "com.github.tminglei" %% "slick-pg_jts" % "0.17.2",
  "org.postgresql" % "postgresql" % "42.2.5",
  "com.sksamuel.elastic4s" %% "elastic4s-core" % versions.elastic excludeAll(
    ExclusionRule(organization = "com.fasterxml.jackson.core"),
    ExclusionRule(organization = "com.fasterxml.jackson.dataformat"),
    ExclusionRule(organization = "com.fasterxml.jackson.datatype"),
    ExclusionRule(organization = "com.fasterxml.jackson.module")
  ),
  "com.sksamuel.elastic4s" %% "elastic4s-json-jackson" % versions.elastic excludeAll(
    ExclusionRule(organization = "com.fasterxml.jackson.core"),
    ExclusionRule(organization = "com.fasterxml.jackson.dataformat"),
    ExclusionRule(organization = "com.fasterxml.jackson.datatype"),
    ExclusionRule(organization = "com.fasterxml.jackson.module")
  ),
  "com.sksamuel.elastic4s" %% "elastic4s-client-esjava" % versions.elastic excludeAll(
    ExclusionRule(organization = "com.fasterxml.jackson.core"),
    ExclusionRule(organization = "com.fasterxml.jackson.dataformat"),
    ExclusionRule(organization = "com.fasterxml.jackson.datatype"),
    ExclusionRule(organization = "com.fasterxml.jackson.module")
  ),

  // test
  "org.specs2" %% "specs2-core" % versions.specs % Test,
  "org.specs2" %% "specs2-mock" % versions.specs % Test,

  // parsing
  "com.github.tototoshi" %% "scala-csv" % "1.3.5",

  //geo
  "com.google.maps" % "google-maps-services" % "0.9.3",
  "org.wololo" % "jts2geojson" % "0.3.1"

)
