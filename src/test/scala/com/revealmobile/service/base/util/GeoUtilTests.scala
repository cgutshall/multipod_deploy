package com.revealmobile.service.base.util

import com.sksamuel.elastic4s.requests.searches.GeoPoint
import com.sksamuel.elastic4s.requests.searches.queries.geo.Shapes.Polygon
import org.specs2.mock.Mockito
import org.specs2.mutable.Specification

class GeoUtilTests extends Specification with Mockito {

  "A GeoUtil method" should {
    "Return a polygon that defines a bounding box given the northeast and southeast corner coordinates" in {
      val result = GeoUtil.boundingBox(1, 0, 1, 0)
      val northWest = GeoPoint(lat=1,long=0)
      val northEast = GeoPoint(lat=1,long=1)
      val southWest = GeoPoint(lat=0,long=0)
      val southEast = GeoPoint(lat=0,long=1)
      val expected = Polygon(Seq(southEast, northEast, northWest, southWest, southEast), None)
      result should beEqualTo(expected)
    }
    "Extract a coordinate as a double from an integer" in {
      val x: Int = 1
      val result = GeoUtil.extractCoordinate(x)
      result mustEqual(1.0)
    }
    "Extract a coordinate as a double from a Long" in {
      val x: Double = 1L
      val result = GeoUtil.extractCoordinate(x)
      result mustEqual(1.0)
    }
    "Extract a coordinate as a double from a Double" in {
      val x: Double = 1D
      val result = GeoUtil.extractCoordinate(x)
      result mustEqual(1.0)
    }
    "Throw an exception if an attempt is made to extract a coordinate from non-numeric type" in {
      val x: String = "I should fail"
      GeoUtil.extractCoordinate(x) must throwA[RuntimeException]
    }
  }

}
