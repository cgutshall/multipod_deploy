package com.revealmobile.service.base.util

import java.io.{ByteArrayInputStream, File}
import java.time.LocalDate
import com.amazonaws.HttpMethod
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.model.{GeneratePresignedUrlRequest, ObjectMetadata, PutObjectRequest, PutObjectResult}

object S3Util {
  def generateSignedUrl(s3: AmazonS3, key: String, bucket: String, expirationDate: LocalDate): String = {
    try {
      val request = new GeneratePresignedUrlRequest(bucket, key)
        .withMethod(HttpMethod.GET)
        .withExpiration(java.sql.Date.valueOf(expirationDate))

      s3.generatePresignedUrl(request).toString

    } catch {
      case e: Exception => e.getMessage
    }
  }

  def upload(s3: AmazonS3, bucket: String, key: String, data: Array[Byte]) = {
    val metadata = new ObjectMetadata()
    metadata.setContentLength(data.length)

    val request = new PutObjectRequest(bucket, key, new ByteArrayInputStream(data), metadata)
    s3.putObject(request)
  }
}
