package com.revealmobile.service.base.util

import com.revealmobile.model.query.SortDirection

object ConversionUtil {
  implicit def toSortDirection(direction: String): SortDirection = {
    if (direction.toLowerCase.startsWith("desc")) {
      SortDirection.Descending
    }
    else {
      SortDirection.Ascending
    }
  }
}
