package com.revealmobile.service.base.util

import com.revealmobile.model.geography.FeatureGeometry
import com.revealmobile.util.JsonUtil
import com.sksamuel.elastic4s.requests.searches.GeoPoint
import com.sksamuel.elastic4s.requests.searches.queries.geo.Shapes.Polygon
import com.vividsolutions.jts.geom.Geometry
import org.wololo.jts2geojson.{GeoJSONReader, GeoJSONWriter}

import scala.math._

object GeoUtil {

  private val EarthRadius = 6371000.0
  private val EarthRadiusMiles = 3959

  //see https://www.movable-type.co.uk/scripts/latlong.html

  //this will produce a distance along the north/south axis, which will be shorter than the distance along east/west the further away from the equator latDegrees is
  def metersToDegreesLat(latDegrees: Double, lonDegrees: Double, distInMeters: Double): Double = {
    val latRadians = latDegrees * Math.PI / 180
    val lat2 = Math.asin( Math.sin(latRadians)*Math.cos(distInMeters/EarthRadius) +
      Math.cos(latRadians)*Math.sin(distInMeters/EarthRadius)*Math.cos(0))
    Math.abs(((lat2 * 180)/Math.PI) - latDegrees)
  }

  //this will produce a distance along east/west, which will be longer than the distance along north/south the further away from the equator latDegrees is
  def metersToDegrees(latDegrees: Double, lonDegrees: Double, distInMeters: Double): Double = {
    val latRadians = latDegrees * Math.PI / 180
    val lonRadians = lonDegrees * Math.PI / 180
    val lat2 = Math.asin( Math.sin(latRadians)*Math.cos(distInMeters/EarthRadius) +
      Math.cos(latRadians)*Math.sin(distInMeters/EarthRadius)*Math.cos(Math.toRadians(90)))
    val lon2 = lonRadians + Math.atan2(Math.sin(Math.toRadians(90))*Math.sin(distInMeters/EarthRadius)*Math.cos(latRadians),
      Math.cos(distInMeters/EarthRadius)-Math.sin(latRadians)*Math.sin(lat2))
    Math.abs(((lon2 * 180)/Math.PI) - lonDegrees)
  }

  def extractCoordinate(coord: Any): Double = coord match {
    case i: Int => i.toDouble
    case l: Long => l.toDouble
    case d: Double => d
    case _ => throw new RuntimeException(s"Problem extracting coordinate: ${coord.toString}")
  }

  def boundingBox(latitude: Double, longitude: Double, proximity: Double): Polygon = {

    val proximityDegreesLat = GeoUtil.metersToDegreesLat(latitude, longitude, proximity * 1000.0)
    val proximityDegreesLong = GeoUtil.metersToDegrees(latitude, longitude, proximity * 1000.0)

    val northEast =  GeoPoint(
      lat=latitude + proximityDegreesLat,
      long=longitude + proximityDegreesLong
    )
    val southEast  = GeoPoint(
      lat=latitude + proximityDegreesLat,
      long=longitude - proximityDegreesLong
    )
    val northWest  = GeoPoint(
      lat=latitude - proximityDegreesLat,
      long=longitude + proximityDegreesLong
    )
    val southWest  = GeoPoint(
      lat=latitude - proximityDegreesLat,
      long=longitude - proximityDegreesLong
    )
    //OSG standard counterclockwise for outer ring
    Polygon(Seq(southEast, northEast, northWest, southWest, southEast), None)
  }

  def boundingBox(north: Double, south: Double, east: Double, west: Double): Polygon = {

    val northEast =  GeoPoint(
      lat=north,
      long=east
    )

    val southEast  = GeoPoint(
      lat=south,
      long=east
    )

    val northWest  = GeoPoint(
      lat=north,
      long=west
    )

    val southWest  = GeoPoint(
      lat=south,
      long=west
    )
    //OSG standard counterclockwise for outer ring
    Polygon(Seq(southEast, northEast, northWest, southWest, southEast), None)
  }

  def toGeometry(geom: FeatureGeometry): Geometry = {
    val json = JsonUtil.SnakeCaseMapper.writeValueAsString(geom)
    val reader = new GeoJSONReader()
    val geometry = reader.read(json)
    geometry.setSRID(4326)
    geometry
  }

  def toFeatureGeometry(geom: Geometry): FeatureGeometry = {
    val writer = new GeoJSONWriter()
    val json = writer.write(geom).toString
    JsonUtil.SnakeCaseMapper.readValue[FeatureGeometry](json)
  }

  def getArea(geom: FeatureGeometry): Double = {
    toGeometry(geom).getArea
  }

  @deprecated
  def distance(lat1: Double, lon1: Double, lat2: Double, lon2: Double): Double = {
    val dLat = (lat2 - lat1).toRadians
    val dLon = (lon2 - lon1).toRadians

    val a = pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(lat1.toRadians) * cos(lat2.toRadians)
    val c = 2 * asin(sqrt(a))
    EarthRadiusMiles * c
  }

}
