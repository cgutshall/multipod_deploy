package com.revealmobile.service.base.module.support.elasticsearch

import com.fasterxml.jackson.annotation.{JsonIgnoreProperties, JsonProperty}

@JsonIgnoreProperties(ignoreUnknown = true)
case class IndexResponse(
  @JsonProperty("_id") id: String,
  @JsonProperty("_index") index: String,
  @JsonProperty("_type") `type`: String,
  @JsonProperty("_version") version: Long,
  @JsonProperty("_seq_no") seqNo: Long,
  @JsonProperty("_primary_term") primaryTerm: Long,
  result: String,
  @JsonProperty("forced_refresh") forcedRefresh: Boolean,
  @JsonProperty("_shards") shards: Shards
)