package com.revealmobile.service.base.module.support.cache

import com.revealmobile.model.cache.Item
import com.revealmobile.util.JsonUtil
import com.revealmobile.util.future._
import com.sksamuel.elastic4s.ElasticDsl._
import com.sksamuel.elastic4s.requests.common.RefreshPolicy
import com.sksamuel.elastic4s.requests.count.CountRequest
import com.sksamuel.elastic4s.requests.get.GetRequest
import com.sksamuel.elastic4s.{ElasticClient, _}
import com.twitter.inject.Logging
import com.twitter.util.Future

import scala.util.Try

class Cache(elastic: ElasticClient) extends Logging {
  implicit object ItemIndexable extends Indexable[Item] {
    override def json(t: Item): String = {
      JsonUtil.SnakeCaseMapper.writeValueAsString(t)
    }
  }

  implicit object ItemHitReader extends HitReader[Item] {
    override def read(hit: Hit): Try[Item] = {
      Try(
        JsonUtil.SnakeCaseMapper.readValue[Item](hit.sourceAsBytes)
      )
    }
  }

  def get(key: String): Future[Option[Item]] = {
    val definition = GetRequest(IndexAndType("cache", "_doc"), key)
    elastic.execute(definition).toTwitterFuture.map(_.result.toOpt[Item])
  }

  def put(key: String, value: Any): Future[Boolean] = {
    val item = Item(
      key = key,
      value = JsonUtil.SnakeCaseMapper.writeValueAsString(value)
    )

    val definition = indexInto("cache")
      .doc(item)
      .withId(key)
      .refresh(RefreshPolicy.Immediate)

    elastic.execute(definition).toTwitterFuture.map(_.isSuccess)
  }

  def clearPrefixes(prefixes: Set[String]): Future[Long] = {
    val prefixFilter = prefixes.map { prefix =>
      prefixQuery("key", prefix)
    }

    val definition = deleteByQuery("cache", boolQuery().should(prefixFilter).minimumShouldMatch(1))
    elastic.execute(definition)
      .toTwitterFuture
      .map(_.result.deleted)
  }

  def clear: Future[Long] = {
    val definition = deleteByQuery("cache", matchAllQuery())
    elastic.execute(definition)
      .toTwitterFuture
      .map(_.result.deleted)
  }

  def count: Future[Long] = {
    val definition = CountRequest("cache")
    elastic.execute(definition)
      .toTwitterFuture
      .map(_.result.count)
  }

}


