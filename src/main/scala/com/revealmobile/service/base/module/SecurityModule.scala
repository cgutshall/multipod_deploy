package com.revealmobile.service.base.module

import com.google.inject.Provides
import com.revealmobile.service.base.module.support.security.SecurityStore
import com.twitter.inject.{Logging, TwitterModule}
import javax.inject.Singleton

object SecurityModule extends TwitterModule with Logging {
  @Provides
  @Singleton
  def providesClient: SecurityStore = {
    new SecurityStore()
  }


}
