package com.revealmobile.service.base.module

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ser.impl.{SimpleBeanPropertyFilter, SimpleFilterProvider}
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.twitter.finatra.json.modules.FinatraJacksonModule


object JsonModule extends FinatraJacksonModule {
  private val filter = SimpleBeanPropertyFilter.serializeAllExcept()

  override protected val serializationInclusion: Include = JsonInclude.Include.ALWAYS

  override protected def additionalMapperConfiguration(mapper: ObjectMapper): Unit = {
    super.additionalMapperConfiguration(
      mapper
        .registerModule(new JavaTimeModule)
    )
  }
}