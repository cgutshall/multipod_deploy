package com.revealmobile.service.base.module.support.pipeline.handler

import com.revealmobile.model.event.TypedEvent
import com.twitter.inject.Logging

abstract class BaseEventHandler[T] extends Logging {
  def handle(event: TypedEvent[T]): Unit

  protected def unsupported(event: TypedEvent[T]): Unit = {
    warn(s"Unsupported event [type=${event.`type`}, action=${event.action}]")
  }
}
