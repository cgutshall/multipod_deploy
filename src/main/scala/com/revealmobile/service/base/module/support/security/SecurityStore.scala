package com.revealmobile.service.base.module.support.security

import com.amazonaws.services.secretsmanager.AWSSecretsManagerClientBuilder
import com.amazonaws.services.secretsmanager.model.GetSecretValueRequest
import com.revealmobile.util.JsonUtil

class SecurityStore {
  private val client = AWSSecretsManagerClientBuilder.standard()
    .build

  def getValue(key: String): Map[String, Any] = {
    val request = new GetSecretValueRequest().withSecretId(key)
    val json = client.getSecretValue(request).getSecretString
    JsonUtil.SnakeCaseMapper.readValue[Map[String, Any]](json)
  }
}

