package com.revealmobile.service.base.module.support.exception

import com.revealmobile.model.response.{BasicResponse, Message, MessageLevel}
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.exceptions.ExceptionMapper
import com.twitter.finatra.http.response.ResponseBuilder
import com.twitter.finatra.json.internal.caseclass.exceptions.{CaseClassMappingException, CaseClassValidationException}
import com.twitter.inject.Logging
import javax.inject.{Inject, Singleton}

@Singleton
class CaseClassExceptionMapper @Inject()(response: ResponseBuilder)
  extends ExceptionMapper[CaseClassMappingException] with Logging {

  override def toResponse(request: Request, e: CaseClassMappingException): Response = {
    error(e.getMessage, e)
    response.badRequest.json(errorsResponse(e))
  }

  private def errorsResponse(e: CaseClassMappingException): BasicResponse = {
    val messages = e.errors.toSet[CaseClassValidationException].map { e =>
      val text = if(e.reason.message.contains("field is required")) {
        val fieldName = e.path.prettyString.split("_").map(_.capitalize).mkString(" ")
        s"'${fieldName}' is required."
      }
      else {
        e.reason.message
      }
      Message(e.path.prettyString, MessageLevel.Error, text)
    }
    BasicResponse(messages = messages)
  }
}