package com.revealmobile.service.base.module.support

import com.revealmobile.service.base.module.support.elasticsearch.SearchResponse
import com.revealmobile.util.JsonUtil
import com.twitter.finagle.Http
import com.twitter.finagle.filter.MaskCancelFilter
import com.twitter.finagle.http.{Request, RequestBuilder, Response}
import com.twitter.finagle.service.FailFastFactory.FailFast
import com.twitter.io.Buf
import com.twitter.util.Future

class ElasticsearchClient(nodes: Set[String]) {
  private val elastic = Http.Client()
    .configured(FailFast(false))
    .withSessionQualifier.noFailureAccrual
    .filtered(new MaskCancelFilter[Request, Response])
    .newService(nodes.head)


  def get[T](index: String, id: String)(implicit ev: scala.reflect.Manifest[T]): Future[Option[T]] = {
    val parameters = Map.empty[String, String]
    val request = RequestBuilder()
      .url(Request.queryString(s"http://${nodes.head}/${index}/_doc/${id}", parameters))
      .buildGet()

    elastic(request).map { response =>
      response.statusCode match {
        case 200 =>
          Option(JsonUtil.SnakeCaseMapper.readValue[T](response.contentString))
        case _ =>
          throw new RuntimeException(response.contentString)
      }
    }
  }

  def index[T](index: String, id: String, entity: T): Future[String] = {
    val json = JsonUtil.SnakeCaseMapper.writeValueAsString(entity)

    val request = RequestBuilder()
      .url(Request.queryString(s"http://${nodes.head}/${index}/_doc/${id}"))
      .setHeader("Content-Type", "application/json")
      .buildPost(Buf.Utf8.apply(json))

    elastic(request).map { response =>
      response.statusCode match {
        case 200 | 201 =>
          id
        case _ =>
          throw new RuntimeException(response.contentString)
      }
    }
  }

  def index[T](index: String, entities: Set[T]): Future[String] = {
    val json = entities.map { entity =>
      val sourceJson = JsonUtil.SnakeCaseMapper.writeValueAsString(entity)
      val intermediateEntity = JsonUtil.SnakeCaseMapper.readValue[Map[String, Any]](sourceJson, classOf[Map[String, Any]])
      val actionJson = s"""{"index":{"_index":"${index}","_id":"${intermediateEntity.get("id").orNull}"}}"""

      s"""${actionJson}\n${sourceJson}\n"""
    }

    val request = RequestBuilder()
      .url(Request.queryString(s"http://${nodes.head}/${index}/_bulk?filter_path="))
      .setHeader("Content-Type", "application/x-ndjson")
      .buildPost(Buf.Utf8.apply(json.mkString))

    elastic(request).map { response =>
      response.statusCode match {
        case 200 | 201 =>
          response.contentString
        case _ =>
          throw new RuntimeException(response.contentString)
      }
    }
  }

  def search[T](index: String, criteria: Map[String, Set[String]], limit: Int)(implicit ev: scala.reflect.Manifest[T]): Future[Seq[T]] = {
    val parameters = Map.empty[String, String]

    val json = if (criteria.nonEmpty) {
      val termsJson = criteria.map { c =>
        s"""
          {
            "terms": {
              "${c._1}": [
                "${c._2.mkString("\",\"")}"
              ]
            }
          }
        """
      }

      s"""
        {
          "size": ${limit},
          "query": {
              "bool": {
                "must": [
                  ${termsJson.mkString(",")}
                ]
              }
            }
         }
      """
    }
    else {
      s"""
        {
          "size": ${limit},
          "query": {
            "match_all": {}
          }
        }
      """
    }

    val request = RequestBuilder()
      .url(Request.queryString(s"http://${nodes.head}/${index}/_doc/_search", parameters))
      .setHeader("Content-Type", "application/json")
      .buildPost(Buf.Utf8.apply(json))

    elastic(request).map { response =>
      response.statusCode match {
        case 200 =>
          val x = JsonUtil.SnakeCaseMapper.readValue[SearchResponse[T]](response.contentString)
          x.hits.hits.map(_.src)
        case _ =>
          throw new RuntimeException(response.contentString)
      }
    }
  }

}