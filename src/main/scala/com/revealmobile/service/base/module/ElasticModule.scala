package com.revealmobile.service.base.module

import com.google.inject.Provides
import com.revealmobile.service.base.module.support.ElasticsearchClient
import com.sksamuel.elastic4s.ElasticClient
import com.sksamuel.elastic4s.http.JavaClient
import com.twitter.inject.{Logging, TwitterModule}
import com.typesafe.config.Config
import javax.inject.Singleton
import org.apache.http.HttpHost
import org.apache.http.client.config.RequestConfig
import org.elasticsearch.client.RestClient
import scala.collection.JavaConverters._

object ElasticModule extends TwitterModule with Logging {
  @Provides
  @Singleton
  def providesClient(config: Config): ElasticClient = {
    val hosts = config.getStringList("elastic.hosts").asScala.map { node =>
      new HttpHost(node, config.getInt("elastic.port"), "http")
    }

    val restClient = RestClient.builder(hosts: _*)
      .setRequestConfigCallback((requestConfigBuilder: RequestConfig.Builder) => requestConfigBuilder.setSocketTimeout(45000))
      .build()

    ElasticClient(new JavaClient(restClient))
  }

  @Provides
  @Singleton
  def providesCustomClient(config: Config): ElasticsearchClient = {
    val nodes = config.getStringList("elastic.hosts")
      .asScala
      .toSet
      .map{ host:String => s"${host}:${config.getInt("elastic.port")}"}
    new ElasticsearchClient(nodes)
  }

}
