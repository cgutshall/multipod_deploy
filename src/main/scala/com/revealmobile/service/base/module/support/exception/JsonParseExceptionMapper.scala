package com.revealmobile.service.base.module.support.exception

import com.fasterxml.jackson.core.JsonParseException
import com.revealmobile.model.response.{BasicResponse, Message, MessageLevel}
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.exceptions.ExceptionMapper
import com.twitter.finatra.http.response.ResponseBuilder
import com.twitter.finatra.json.internal.caseclass.jackson.JacksonUtils
import com.twitter.inject.Logging
import javax.inject.{Inject, Singleton}

@Singleton
class JsonParseExceptionMapper@Inject()(response: ResponseBuilder) extends ExceptionMapper[JsonParseException] with Logging {

  override def toResponse(request: Request, e: JsonParseException): Response = {
    error(e.getMessage, e)
    response.badRequest.json(errorsResponse(e))
  }

  private def errorsResponse(e: JsonParseException): BasicResponse = {
    BasicResponse(messages = Set(Message("*", MessageLevel.Error, JacksonUtils.errorMessage(e))))
  }

}