package com.revealmobile.service.base.module.support.exception


import com.revealmobile.model.response.BasicResponse
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.exceptions.ExceptionMapper
import com.twitter.finatra.http.response.ResponseBuilder
import com.twitter.inject.Logging
import javax.inject.{Inject, Singleton}

@Singleton
class RestExceptionMapper @Inject()(response: ResponseBuilder)
  extends ExceptionMapper[RestException] with Logging {

  override def toResponse(request: Request, e: RestException): Response = {
    error(e.messages.mkString(", "))
    response.status(e.status).json(errorsResponse(e))
  }

  private def errorsResponse(e: RestException): BasicResponse = {
    BasicResponse(messages = e.messages)
  }
}