package com.revealmobile.service.base.module

import com.google.inject.Provides
import com.revealmobile.model.event.{EventAction, EventType}
import com.revealmobile.service.base.module.support.pipeline.Producer
import com.twitter.inject.{Logging, TwitterModule}
import com.twitter.util.{ScheduledThreadPoolTimer, _}
import com.typesafe.config.Config
import javax.inject.Singleton

import scala.collection.JavaConverters._

object PipelineModule extends TwitterModule with Logging {
  private val timer = new ScheduledThreadPoolTimer()

  @Provides
  @Singleton
  def providesProducer(config: Config): Producer = {
    val producer = new Producer(config.getStringList("pipeline.kafka").asScala)

    val period = Duration.fromMinutes(5)

    timer.schedule(Time.now, period)({
      producer.send("system", EventType.Periodic, EventAction.Refresh, "1.0", None)
    })

    producer
  }

  override protected def closeOnExit(f: => Unit): Unit = {
    timer.stop()
    super.closeOnExit(f)
  }
}
