package com.revealmobile.service.base.module

import com.revealmobile.model.security.Principal
import com.twitter.inject.TwitterModule
import com.twitter.inject.requestscope.RequestScopeBinding

object UserModule extends TwitterModule with RequestScopeBinding {
  override def configure() {
    bindRequestScope[Principal]
  }
}
