package com.revealmobile.service.base.module.support.exception


import com.revealmobile.model.response.Message
import com.twitter.finagle.http.{MediaType, Status}
import com.twitter.finatra.http.exceptions.HttpException

case class RestException(
  status: Status = Status.BadRequest,
  messages: Set[Message]
) extends HttpException(status, MediaType.JsonUtf8)