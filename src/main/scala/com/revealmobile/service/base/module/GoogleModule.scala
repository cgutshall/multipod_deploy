package com.revealmobile.service.base.module

import com.google.inject.Provides
import com.google.maps.GeoApiContext
import com.twitter.inject.{Logging, TwitterModule}
import com.typesafe.config.Config
import javax.inject.Singleton


object GoogleModule extends TwitterModule with Logging {
  @Singleton
  @Provides
  def providesApiClient(config: Config): GeoApiContext = {
    new GeoApiContext.Builder().apiKey(config.getString("google.api_key")).build()
  }
}
