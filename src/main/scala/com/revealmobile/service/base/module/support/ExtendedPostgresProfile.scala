package com.revealmobile.service.base.module.support


import com.github.tminglei.slickpg._
import slick.driver.JdbcProfile
import slick.profile.Capability

trait ExtendedPostgresProfile extends ExPostgresProfile
  with PgArraySupport
  with PgDateSupport
  with PgDate2Support
  with PgRangeSupport
  with PgHStoreSupport
  with PgSearchSupport
  with PgPostGISSupport
  with PgNetSupport
  with PgLTreeSupport {
  def pgjson = "jsonb" // jsonb support is in postgres 9.4.0 onward; for 9.3.x use "json"

  // Add back `capabilities.insertOrUpdate` to enable native `upsert` support; for postgres 9.5+
  override protected def computeCapabilities: Set[Capability] =
    super.computeCapabilities + JdbcProfile.capabilities.insertOrUpdate

  override val api = MyAPI

  object MyAPI extends API with ArrayImplicits with PostGISAssistants
    with SimpleDateTimeImplicits
    with DateTimeImplicits
    with NetImplicits
    with LTreeImplicits
    with RangeImplicits
    with HStoreImplicits
    with SearchImplicits
    with PostGISImplicits
    with PostGISPlainImplicits
    with Date2DateTimePlainImplicits
    with SearchAssistants {
    implicit val strListTypeMapper = new SimpleArrayJdbcType[String]("text").to(_.toList)

    implicit val listListWitness = ElemWitness.AnyWitness.asInstanceOf[ElemWitness[List[String]]]
    implicit val simpleLListListTypeMapper = new SimpleArrayJdbcType[List[String]]("text")
      .to(_.asInstanceOf[Seq[Array[Any]]].toList.map(_.toList.asInstanceOf[List[String]]))
  }

}


object ExtendedPostgresProfile extends ExtendedPostgresProfile
