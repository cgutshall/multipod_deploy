package com.revealmobile.service.base.module

import com.google.inject.Provides
import com.revealmobile.service.base.module.support.security.SecurityStore
import com.twitter.inject.{Logging, TwitterModule}
import com.typesafe.config.{Config, ConfigFactory, ConfigValueFactory}
import javax.inject.Singleton
import scala.collection.JavaConverters._


object ConfigurationModule extends TwitterModule with Logging {
  @Provides
  @Singleton
  def providesEnvironmentConfiguration(securityStore: SecurityStore): Config = {
    // resolve environment
    val environment = scala.util.Properties.envOrElse("ENVIRONMENT", "local")
    info("Environment: " + environment)

    // define base configuration
    val baseConfig = ConfigFactory.load()
    val configuration = baseConfig.getConfig(environment)
      .withFallback(baseConfig)

    // resolve external settings
    if(!configuration.entrySet.asScala.exists(_.getKey.contains("database"))) {
      configuration
    }
    else {
      val secretsManagerKey = configuration.getString("database.secrets_manager_key")
      val dbConfiguration = securityStore.getValue(secretsManagerKey).foldLeft(configuration) { (k, v) =>
        val key = v._1 match {
          case "dbname" => "properties.databaseName"
          case "username" => "properties.user"
          case "password" => "properties.password"
          case "host" => "properties.serverName"
          case _ => v._1
        }
        k.withValue(s"database.${key}", ConfigValueFactory.fromAnyRef(v._2))
      }
      dbConfiguration
    }
  }

}
