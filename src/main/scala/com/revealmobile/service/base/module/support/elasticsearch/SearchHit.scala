package com.revealmobile.service.base.module.support.elasticsearch

import com.fasterxml.jackson.annotation.{JsonIgnoreProperties, JsonProperty}

@JsonIgnoreProperties(ignoreUnknown = true)
case class SearchHit[T](
  @JsonProperty("_id") id: String,
  @JsonProperty("_index") index: String,
  @JsonProperty("_source") src: T
)
