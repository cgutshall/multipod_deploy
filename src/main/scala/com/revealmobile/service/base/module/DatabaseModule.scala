package com.revealmobile.service.base.module

import com.google.inject.Provides
import com.twitter.inject.{Logging, TwitterModule}
import com.typesafe.config.Config

import javax.inject.Singleton
import com.revealmobile.service.base.module.support.ExtendedPostgresProfile
import com.revealmobile.service.base.module.support.ExtendedPostgresProfile.api._

object DatabaseModule extends TwitterModule with Logging {
  @Singleton
  @Provides
  def providesDatabaseClient(config: Config): ExtendedPostgresProfile.backend.DatabaseDef = {
    Database.forConfig(path = "database", config = config)
  }
}
