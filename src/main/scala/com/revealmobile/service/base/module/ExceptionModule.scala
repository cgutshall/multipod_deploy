package com.revealmobile.service.base.module

import com.revealmobile.service.base.module.support.exception._
import com.twitter.finatra.http.exceptions.ExceptionManager
import com.twitter.inject.{Injector, TwitterModule}


object ExceptionModule extends TwitterModule {
  override def singletonStartup(injector: Injector) {
    val manager = injector.instance[ExceptionManager]
    manager.add[JsonParseExceptionMapper]
    manager.add[CaseClassExceptionMapper]
    manager.add[RestExceptionMapper]
    manager.add[HttpExceptionMapper]
    manager.add[ThrowableExceptionMapper]
  }
}