package com.revealmobile.service.base.module.support.elasticsearch

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
case class SearchHits[T](
  hits: Seq[SearchHit[T]]
)
