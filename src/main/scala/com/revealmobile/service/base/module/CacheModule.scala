package com.revealmobile.service.base.module

import com.google.inject.Provides
import com.revealmobile.service.base.module.support.cache.Cache
import com.sksamuel.elastic4s.ElasticClient
import com.twitter.inject.{Logging, TwitterModule}
import javax.inject.Singleton

object CacheModule extends TwitterModule with Logging {
  @Singleton
  @Provides
  def providesCache(elastic: ElasticClient): Cache = {
    new Cache(elastic)
  }

}
