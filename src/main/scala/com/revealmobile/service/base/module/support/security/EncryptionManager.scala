package com.revealmobile.service.base.module.support.security

import java.nio.ByteBuffer
import java.util.Base64

import com.amazonaws.services.kms.model.DecryptRequest
import com.amazonaws.services.kms.{AWSKMS, AWSKMSClientBuilder}
import com.google.crypto.tink.aead.AeadConfig
import com.google.crypto.tink.{Aead, CleartextKeysetHandle, JsonKeysetReader, KeysetHandle}
import com.typesafe.config.Config
import javax.inject.Inject

class EncryptionManager @Inject()(configuration: Config) {

  private val encriptionKey: String = configuration.getString("security.encryption")
  private val aeadKeySet: String = configuration.getString("security.aead_key_set")

  private val kmsClient: AWSKMS = AWSKMSClientBuilder.defaultClient

  private val ciphertextBlob: ByteBuffer  = ByteBuffer.wrap(Base64.getDecoder.decode(encriptionKey))

  private val decryptRequest: DecryptRequest = new DecryptRequest().withCiphertextBlob(ciphertextBlob)

  //Tink initialization.
  AeadConfig.register()

  private val keysetHandle: KeysetHandle = CleartextKeysetHandle.read(JsonKeysetReader.withString(aeadKeySet))


  private val aead: Aead = keysetHandle.getPrimitive(classOf[Aead])

  def encrypt(value: String): String = {
    val plainByteBuffer: ByteBuffer = kmsClient.decrypt(decryptRequest).getPlaintext()
    val encrypted: Array[Byte] = aead.encrypt(value.getBytes(), plainByteBuffer.array())
    Base64.getEncoder.encodeToString(encrypted)
  }

  def decrypt(value: String): String = {
    val plainByteBuffer: ByteBuffer = kmsClient.decrypt(decryptRequest).getPlaintext()
    val encrypted: Array[Byte] = Base64.getDecoder.decode(value)
    new String(aead.decrypt(encrypted, plainByteBuffer.array()))
  }
}
