package com.revealmobile.service.base.module.support.elasticsearch

import com.fasterxml.jackson.annotation.JsonIgnoreProperties

@JsonIgnoreProperties(ignoreUnknown = true)
case class SearchResponse[T](
  hits: SearchHits[T]
)