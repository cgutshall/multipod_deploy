package com.revealmobile.service.base.module.support.pipeline

import java.util.concurrent.Future
import java.util.{Properties, UUID}

import com.revealmobile.model.event.{Event, EventAction, EventType}
import com.revealmobile.util.JsonUtil
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord, RecordMetadata}
import org.slf4j.LoggerFactory

class Producer(bootstrapServers: Seq[String]) extends Serializable{
  private val log = LoggerFactory.getLogger(getClass)
  log.debug("Starting producer")

  private val props:Properties = new Properties()
  props.put("bootstrap.servers", bootstrapServers.mkString(","))
  props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("acks","all")
  val producer = new KafkaProducer[String, String](props)

  def send(topic: String, `type`: EventType, action: EventAction, version: String, source: Any, key: Option[String] = None): Future[RecordMetadata] = {
    val event = Event(
      `type` = `type`,
      action = action,
      version = version,
      source = source
    )

    val json = JsonUtil.SnakeCaseMapper.writeValueAsString(event)

    val record = new ProducerRecord[String, String](topic, key.getOrElse(UUID.randomUUID.toString), json)

    producer.send(record)
  }

  def close() = {
    log.info("Closing producer")
    producer.close()
  }
}

