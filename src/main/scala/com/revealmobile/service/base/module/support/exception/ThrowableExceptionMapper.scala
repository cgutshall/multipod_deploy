package com.revealmobile.service.base.module.support.exception

import com.revealmobile.model.response.{BasicResponse, Message, MessageLevel}
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finatra.http.exceptions.ExceptionMapper
import com.twitter.finatra.http.response.ResponseBuilder
import com.twitter.inject.Logging
import javax.inject.{Inject, Singleton}

@Singleton
class ThrowableExceptionMapper @Inject()(response: ResponseBuilder)
  extends ExceptionMapper[Throwable] with Logging {

  override def toResponse(request: Request, e: Throwable): Response = {
    error(e.getMessage, e)

    val message = Message(
      context = "*",
      level = MessageLevel.Error,
      text = "An unexpected error has occurred."
    )

    response.internalServerError.json(BasicResponse(messages = Set(message)))
  }
}