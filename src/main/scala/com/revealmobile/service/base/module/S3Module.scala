package com.revealmobile.service.base.module

import com.amazonaws.services.s3.{AmazonS3, AmazonS3ClientBuilder}
import com.google.inject.Provides
import com.twitter.inject.{Logging, TwitterModule}
import com.typesafe.config.Config
import javax.inject.Singleton

object S3Module extends TwitterModule with Logging {
  @Provides
  @Singleton
  def providesClient(config: Config): AmazonS3 = {
    AmazonS3ClientBuilder
      .standard()
      .build()
  }


}
