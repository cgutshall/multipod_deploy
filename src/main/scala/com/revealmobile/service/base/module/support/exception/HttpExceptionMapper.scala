package com.revealmobile.service.base.module.support.exception

import javax.inject.{Inject, Singleton}
import com.revealmobile.model.response.{BasicResponse, Message, MessageLevel}
import com.twitter.finagle.http.{MediaType, Request, Response}
import com.twitter.finatra.http.exceptions.{ExceptionMapper, HttpException}
import com.twitter.finatra.http.response.ResponseBuilder
import com.twitter.inject.Logging

@Singleton
class HttpExceptionMapper @Inject()(response: ResponseBuilder)
  extends ExceptionMapper[HttpException] with Logging {

  override def toResponse(
    request: Request,
    e: HttpException
  ): Response = {
    error(e.getMessage(), e)

    val builder = response
      .status(e.statusCode)

    if (e.mediaType.equals(MediaType.JsonUtf8)) {
      val errors = e.errors.zipWithIndex.map(e => ((e._2 + 1).toString, e._1))
      builder.json(BasicResponse(messages = errors.toSet[(String, String)].map(e => Message(e._1, MessageLevel.Error, e._2))))
    }
    else {
      builder.plain(e.errors.mkString(", "))
    }
  }
}