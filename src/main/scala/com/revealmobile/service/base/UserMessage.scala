package com.revealmobile.service.base

import com.revealmobile.model.response.Message

object UserMessage {
  // security
  val AccessDenied = Message(text = "Access denied")
}
