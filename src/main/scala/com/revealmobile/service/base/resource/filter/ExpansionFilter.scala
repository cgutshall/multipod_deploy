package com.revealmobile.service.base.resource.filter

import com.revealmobile.service.base.resource.filter.ExpansionFilter._
import com.twitter.finagle.http.{Request, Response}
import com.twitter.finagle.{Service, SimpleFilter}
import com.twitter.finatra.json.FinatraObjectMapper
import com.twitter.util.Future
import javax.inject.Inject

/**
  * 1. Passes the request through unaltered
  * 2. Parses a response of type RestResponse
  * 3. lifts the "expansions" subtree in the data field to the root level of the data field
  * 4. removes the expansions entry from the data field
  * 5. replaces the response body with the updated value from steps 3 and 4, leaving the message and errors fields untouched
  */
class ExpansionFilter @Inject()(finatraObjectMapper: FinatraObjectMapper) extends SimpleFilter[Request, Response] {

  override def apply(request: Request, service: Service[Request, Response]): Future[Response] = {
    service(request) map { response =>
      val updatedResp = removeExpansionField(response.contentString, finatraObjectMapper)
      response.setContentString(updatedResp)
      response
    }
  }
}

object ExpansionFilter {

  def removeExpansionField(json: String, finatraObjectMapper: FinatraObjectMapper): String = {
    val resp = finatraObjectMapper.parse[Map[String, Any]](json)
    val isSeq = json.contains(""""data":[""")

    val data = if(isSeq) {
      resp.get("data").map(data => finatraObjectMapper.convert[Seq[Any]](data)).getOrElse(Seq.empty)
    }
    else {
      Seq(resp("data"))
    }

    if(data != null) {
      val updatedBody = data.map {
        case entry: Map[String @unchecked, Any @unchecked] =>
          val expansions = entry.get("expansions").map(_.asInstanceOf[Map[String, Any]]).getOrElse(Map.empty[String, Any])
          val updatedEntry = expansions.map {
            case (key, value) =>
              key -> expansions.getOrElse(key, value)
          }
          (entry ++ updatedEntry) - "expansions"
        case entry =>
          entry
      }

      val updatedData = if(isSeq) { updatedBody} else { updatedBody.headOption}
      val updatedResp = resp ++ Map("data" -> updatedData)
      finatraObjectMapper.writeValueAsString(updatedResp)
    }
    else {
      json
    }

  }

}