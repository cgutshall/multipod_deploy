package com.revealmobile.service.base.resource.filter

import com.revealmobile.model.response.BasicResponse
import com.revealmobile.model.security.Principal
import com.revealmobile.model.security.jwt.Claim
import com.revealmobile.service.base.UserMessage
import com.revealmobile.util.JsonUtil
import com.twitter.finagle.http.{Fields, Request, Response}
import com.twitter.finagle.{Service, SimpleFilter}
import com.twitter.finatra.http.response.ResponseBuilder
import com.twitter.inject.Logging
import com.twitter.inject.requestscope.FinagleRequestScope
import com.twitter.util.Future
import javax.inject.{Inject, Singleton}
import pdi.jwt.{Jwt, JwtAlgorithm}

@Singleton
class UserFilter @Inject()(requestScope: FinagleRequestScope)(responseBuilder: ResponseBuilder)
  extends SimpleFilter[Request, Response] with Logging {

  private val UnauthorizedResponse = responseBuilder.unauthorized(BasicResponse(messages = Set(UserMessage.AccessDenied))).toFuture
  private val TokenPrefix = "Bearer "
  private val SecretKey = "296nx4bpc6dmeixpqbg7nb56dja4eqr24cnfjrey"

  def apply(request: Request, service: Service[Request, Response]): Future[Response] = {
    // extract jwt from authorization header
    val header = request
      .headerMap
      .get(Fields.Authorization)
      .map(_.replaceAll(TokenPrefix, ""))

    val claim = header.flatMap { t =>
      Jwt.decode(t, SecretKey, Seq(JwtAlgorithm.HS256)).toOption.map { decoded =>
        JsonUtil.SnakeCaseMapper.readValue[Claim](decoded)
      }

    }

    // add principal to request scope or error
    claim match {
      case Some(c) =>
        requestScope.seed[Principal](c.principal)
        service(request)
      case None =>
        UnauthorizedResponse
    }
  }
}
